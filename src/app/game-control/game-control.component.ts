import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {

  ref: number;
  @Output() scoreboardStart = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  onStartGame() {
    this.ref = setInterval(() => {
      this.scoreboardStart.emit();
    }, 1000);
  }

  onFinishGame() {
    clearInterval(this.ref);
  }
}
