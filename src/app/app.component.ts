import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  scoreboard = [];

  onScoreboardStart(event) {
    this.scoreboard.push(this.scoreboard.length);
  }

}
